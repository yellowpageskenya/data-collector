$(document).ready(function() {
    // remove input placeholder
    $(':input').removeAttr('placeholder');

    belong_to = $("#belong_to_any_association select").attr("value");
    console.log(belong_to);
    // if (belong_to == "Yes") {
    //     $("association_name").attr("required", 'required');
    // }

    // catch start time when the form opens
    var d = new Date(),
        h = d.getHours(),
        m = d.getMinutes();
    if (h < 10) h = '0' + h;
    if (m < 10) m = '0' + m;
    $('input[type="time"][value="now"]').each(function() {
        $(this).attr({
            'value': h + ':' + m
        });
    });

    // Check duplicate input values
    // $("input").change(function() {
    //     var x = $(this).val();
    //     var z = 0;
    //     $("input").each(function() {
    //         var y = $(this).val();
    //         if (x == y) {
    //             z = z + 1;
    //         }
    //     });
    //     if (z > 1) {
    //         alert(x);
    //     }
    // })

    // hide default location
    $('#location').hide();

    // append business owner title div
    $("<div class='col-12 col-md-12 col-sm-12 col-lg-12'><h4>Business Owner/Manager Contact Details</h4></div>").insertBefore("#business_owner_name");

    // append Business Contact Details title div
    $("<div class='col-12 col-md-12 col-sm-12 col-lg-12'><h4>Business Contact Details</h4></div>").insertBefore("#phone_number");

    $('#tpsf_clusters').hide();
    $('#belong_to_any_association').hide();
    $('#association_name').hide();
    $("#member_of_TPSF select").change(function() {
        $(this).find("option:selected").each(function() {
            var value = $(this).attr("value");
            console.log(value);
            if (value == "Yes") {
                $('#belong_to_any_association').hide();
                $('#tpsf_clusters').show();
                $('#association_name').hide();
            } else {
                $('#tpsf_clusters').hide();
                $('#belong_to_any_association').show();
            }

        });
    }).change();

    $("#belong_to_any_association select").change(function() {
        $(this).find("option:selected").each(function() {
            var value = $(this).attr("value");
            if (value == "Yes") {
                $('#association_name').show();
                $("#association_name input").attr("required", true);
            } else {
                $('#association_name').hide();
                $("#association_name input").attr("required", false);
            }

        });
    }).change();


    $("#member_of_TPSF select").change(function() {
        $(this).find("option:selected").each(function() {
            var value = $(this).attr("value");
            if (value == "Yes") {
                $("#tpsf_clusters select").attr("required", true);
            } else {
                $("#tpsf_clusters select").attr("required", false);
            }

        });
    }).change();


    $("#district select").change(function() {
        $(this).find("option:selected").each(function() {
            var district_id = $(this).attr("value");
            console.log(district_id);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name="_token"]').attr('content')
                }
            });

            $.ajax({
                url: '/getWards',
                method: 'GET',
                data: {
                    district_id: district_id
                },
                success: function(data) {
                    if (data.status == 'ok') {
                        $('#ward select').val(null).empty().select2('destroy');
                        $('#ward select').select2();
                        results = data.results;
                        // console.log(results);
                        var obj = jQuery.parseJSON(JSON.stringify(results));
                        $.each(obj, function(key, value) {
                            console.log(value.id);
                            var data = {
                                id: value.id,
                                text: value.name
                            };

                            var newOption = new Option(data.text, data.id, false, false);
                            $('#ward select').append(newOption).trigger('change');
                        });
                    } else {

                    }
                },
                fail: function() {
                    alert('NO');
                }
            });
        });
    }).change();

    $("#business_operate_known_building select").change(function() {
        $(this).find("option:selected").each(function() {
            var optionValue = $(this).attr("value");
            console.log(optionValue);
            if (optionValue == 'open_space') {
                $("#physical_address").hide();
                $("#floor").hide();
                $("#street_address").hide();
                $("input:hidden[name='open_space_description']").attr("type", 'text');
                $("#open_space_description").css('display', 'block !important');
                $("#open_space_description").removeClass("hidden");
                $("#open_space_description input").attr("required", true);
                $("#physical_address input").attr("required", false);
                $("#street_address input").attr("required", false);
                $("#floor input").attr("required", false);

            } else {
                $("#open_space_description").addClass("hidden");
                $("#open_space_description input").attr("required", false);
                $("#physical_address").show();
                $("#physical_address input").attr("required", true);
                $("#street_address").show();
                $("#street_address input").attr("required", true);
                $("#floor").show();
                $("#floor input").attr("required", true);
            }
        });
    }).change();

    // Phone Duplicates Validation
    $("#business_owner_number input").on("change paste keyup", function() {
        limitText(this, 9);
        number = $(this).val();
        console.log(number)
        $(this).val(parseInt(number))
            // count = number.replace(/ /g, '').length;
            // if (count > 8) {
            //     $.ajax({
            //         url: '/checkDuplicates',
            //         method: 'GET',
            //         data: {
            //             term: number
            //         },
            //         success: function(data) {
            //             console.log(data.error)
            //             if (data.error === 'true') {
            //                 $('#business_owner_number input').get(0).focus();
            //                 $("#business_owner_number").css({ "border": "double", "border-color": "red" });
            //                 $('<div class="business_owner_number alert alert-danger" role="alert">' + data.msg + '</div>').insertAfter("#business_owner_number label");
            //                 setTimeout(function() {
            //                     $(".business_owner_number").fadeOut("slow");
            //                 }, 5000)
            //                 $(".form-edit-add :submit").attr("disabled", true);
            //             } else {
            //                 $("#business_owner_number").css({ "border": "none" });
            //                 $(".form-edit-add :submit").removeAttr("disabled");
            //             }
            //         },
            //         fail: function() {
            //             alert('NO');
            //         }
            //     });
            // }
    }).change();

    $("#phone_number input").on("change paste keyup", function() {
        limitText(this, 9);
        number = $(this).val();
        console.log(number)
        $(this).val(parseInt(number))
            // count = number.replace(/ /g, '').length;
            // if (count > 8) {
            //     $.ajax({
            //         url: '/checkDuplicates',
            //         method: 'GET',
            //         data: {
            //             term: number
            //         },
            //         success: function(data) {
            //             console.log(data.error)
            //             if (data.error === 'true') {
            //                 $('#phone_number input').get(0).focus();
            //                 $("#phone_number").css({ "border": "double", "border-color": "red" });
            //                 $('<div class="phone_number alert alert-danger" role="alert">' + data.msg + '</div>').insertAfter("#phone_number label");
            //                 setTimeout(function() {
            //                     $(".phone_number").fadeOut("slow");
            //                 }, 5000)
            //                 $(".form-edit-add :submit").attr("disabled", true);
            //             } else {
            //                 $("#phone_number").css({ "border": "none" });
            //                 $(".form-edit-add :submit").removeAttr("disabled");
            //             }
            //         },
            //         fail: function() {
            //             alert('NO');
            //         }
            //     });
            // }
    }).change();

    $("#other_phone_number_1 input").on("change paste keyup", function() {
        limitText(this, 9);
        number = $(this).val();
        console.log(number)
        $(this).val(parseInt(number))
            // count = number.replace(/ /g, '').length;
            // if (count > 8) {
            //     $.ajax({
            //         url: '/checkDuplicates',
            //         method: 'GET',
            //         data: {
            //             term: number
            //         },
            //         success: function(data) {
            //             console.log(data.error)
            //             if (data.error === 'true') {
            //                 $('#other_phone_number_1 input').get(0).focus();
            //                 $("#other_phone_number_1").css({ "border": "double", "border-color": "red" });
            //                 $('<div class="other_phone_number_1 alert alert-danger" role="alert">' + data.msg + '</div>').insertAfter("#other_phone_number_1 label");
            //                 setTimeout(function() {
            //                     $(".other_phone_number_1").fadeOut("slow");
            //                 }, 5000)
            //                 $(".form-edit-add :submit").attr("disabled", true);
            //             } else {
            //                 $("#other_phone_number_1").css({ "border": "none" });
            //                 $(".form-edit-add :submit").removeAttr("disabled");
            //             }
            //         },
            //         fail: function() {
            //             alert('NO');
            //         }
            //     });
            // }
    }).change();

    $("#other_phone_number_2 input").on("change paste keyup", function() {
        limitText(this, 9);
        number = $(this).val();
        console.log(number)
        $(this).val(parseInt(number))
            // count = number.replace(/ /g, '').length;
            // if (count > 8) {
            //     $.ajax({
            //         url: '/checkDuplicates',
            //         method: 'GET',
            //         data: {
            //             term: number
            //         },
            //         success: function(data) {
            //             console.log(data.error)
            //             if (data.error === 'true') {
            //                 $('#other_phone_number_2 input').get(0).focus();
            //                 $("#other_phone_number_2").css({ "border": "double", "border-color": "red" });
            //                 $('<div class="other_phone_number_2 alert alert-danger" role="alert">' + data.msg + '</div>').insertAfter("#other_phone_number_2 label");
            //                 setTimeout(function() {
            //                     $(".other_phone_number_2").fadeOut("slow");
            //                 }, 5000)
            //                 $(".form-edit-add :submit").attr("disabled", true);
            //             } else {
            //                 $(".form-edit-add :submit").removeAttr("disabled");
            //                 $("#other_phone_number_2").css({ "border": "none" });
            //             }
            //         },
            //         fail: function() {
            //             alert('Ajax Request Failed');
            //         }
            //     });
            // }
    }).change();

    $("#telephone_number input").on("change paste keyup", function() {
        limitText(this, 9);
        number = $(this).val();
        console.log(number)
        $(this).val(parseInt(number))
            // count = number.replace(/ /g, '').length;
            // if (count > 8) {
            //     $.ajax({
            //         url: '/checkDuplicates',
            //         method: 'GET',
            //         data: {
            //             term: number
            //         },
            //         success: function(data) {
            //             console.log(data.error)
            //             if (data.error === 'true') {
            //                 $('#telephone_number input').get(0).focus();
            //                 $("#telephone_number").css({ "border": "double", "border-color": "red" });
            //                 $('<div class="telephone_number alert alert-danger" role="alert">' + data.msg + '</div>').insertAfter("#telephone_number label");
            //                 setTimeout(function() {
            //                     $(".telephone_number").fadeOut("slow");
            //                 }, 5000)
            //                 $(".form-edit-add :submit").attr("disabled", true);
            //             } else {
            //                 $("#telephone_number").css({ "border": "none" });
            //                 $(".form-edit-add :submit").removeAttr("disabled");
            //             }
            //         },
            //         fail: function() {
            //             alert('NO');
            //         }
            //     });
            // }
    }).change();

    $("#other_telephone_number_1 input").on("change paste keyup", function() {
        limitText(this, 9);
        number = $(this).val();
        console.log(number)
        $(this).val(parseInt(number))
            // count = number.replace(/ /g, '').length;
            // if (count > 8) {
            //     $.ajax({
            //         url: '/checkDuplicates',
            //         method: 'GET',
            //         data: {
            //             term: number
            //         },
            //         success: function(data) {
            //             console.log(data.error)
            //             if (data.error === 'true') {
            //                 $('#other_telephone_number_1 input').get(0).focus();
            //                 $("#other_telephone_number_1").css({ "border": "double", "border-color": "red" });
            //                 $('<div class="other_telephone_number_1 alert alert-danger" role="alert">' + data.msg + '</div>').insertAfter("#other_telephone_number_1 label");
            //                 setTimeout(function() {
            //                     $(".other_telephone_number_1").fadeOut("slow");
            //                 }, 5000)
            //                 $(".form-edit-add :submit").attr("disabled", true);
            //             } else {
            //                 $(".form-edit-add :submit").removeAttr("disabled");
            //                 $("#other_telephone_number_1").css({ "border": "none" });
            //             }
            //         },
            //         fail: function() {
            //             alert('Ajax Request Failed');
            //         }
            //     });
            // }
    }).change();

    $("#other_telephone_number_2 input").on("change paste keyup", function() {
        limitText(this, 9);
        number = $(this).val();
        console.log(number)
        $(this).val(parseInt(number))
            // count = number.replace(/ /g, '').length;
            // if (count > 8) {
            //     $.ajax({
            //         url: '/checkDuplicates',
            //         method: 'GET',
            //         data: {
            //             term: number
            //         },
            //         success: function(data) {
            //             console.log(data.error)
            //             if (data.error === 'true') {
            //                 $('#other_telephone_number_2 input').get(0).focus();
            //                 $("#other_telephone_number_2").css({ "border": "double", "border-color": "red" });
            //                 $('<div class="other_telephone_number_2 alert alert-danger" role="alert">' + data.msg + '</div>').insertAfter("#other_telephone_number_2 label");
            //                 setTimeout(function() {
            //                     $(".other_telephone_number_2").fadeOut("slow");
            //                 }, 5000)
            //                 $(".form-edit-add :submit").attr("disabled", true);
            //             } else {
            //                 $(".form-edit-add :submit").removeAttr("disabled");
            //                 $("#other_telephone_number_2").css({ "border": "none" });
            //             }
            //         },
            //         fail: function() {
            //             alert('Ajax Request Failed');
            //         }
            //     });
            // }
    }).change();

    // Email Duplicates Validation
    $("#email input").on("change paste keyup", function() {
        number = $(this).val();
        console.log(number)
            // count = number.replace(/ /g, '').length;
            // if (count > 5) {
            //     $.ajax({
            //         url: '/checkDuplicates',
            //         method: 'GET',
            //         data: {
            //             term: number
            //         },
            //         success: function(data) {
            //             console.log(data.error)
            //             if (data.error === 'true') {
            //                 $('#email input').get(0).focus();
            //                 $("#email").css({ "border": "double", "border-color": "red" });
            //                 $('<div class="email alert alert-danger" role="alert">' + data.msg + '</div>').insertAfter("#email label");
            //                 setTimeout(function() {
            //                     $(".email").fadeOut("slow");
            //                 }, 5000)
            //                 $(".form-edit-add :submit").attr("disabled", true);
            //             } else {
            //                 $(".form-edit-add :submit").removeAttr("disabled");
            //                 $("#email").css({ "border": "none" });
            //             }
            //         },
            //         fail: function() {
            //             alert('Ajax Request Failed');
            //         }
            //     });
            // }
    }).change();

    $("#other_email input").on("change paste keyup", function() {
        number = $(this).val();
        console.log(number)
            // count = number.replace(/ /g, '').length;
            // if (count > 8) {
            //     $.ajax({
            //         url: '/checkDuplicates',
            //         method: 'GET',
            //         data: {
            //             term: number
            //         },
            //         success: function(data) {
            //             console.log(data.error)
            //             if (data.error === 'true') {
            //                 $('#other_email input').get(0).focus();
            //                 $("#other_email").css({ "border": "double", "border-color": "red" });
            //                 $('<div class="other_email alert alert-danger" role="alert">' + data.msg + '</div>').insertAfter("#other_email label");
            //                 setTimeout(function() {
            //                     $(".other_email").fadeOut("slow");
            //                 }, 5000)
            //                 $(".form-edit-add :submit").attr("disabled", true);
            //             } else {
            //                 $(".form-edit-add :submit").removeAttr("disabled");
            //                 $("#other_email").css({ "border": "none" });
            //             }
            //         },
            //         fail: function() {
            //             alert('Ajax Request Failed');
            //         }
            //     });
            // }
    }).change();

    $("#other_email_2 input").on("change paste keyup", function() {
        number = $(this).val();
        console.log(number)
            // count = number.replace(/ /g, '').length;
            // if (count > 8) {
            //     $.ajax({
            //         url: '/checkDuplicates',
            //         method: 'GET',
            //         data: {
            //             term: number
            //         },
            //         success: function(data) {
            //             console.log(data.error)
            //             if (data.error === 'true') {
            //                 $('#other_email_2 input').get(0).focus();
            //                 $("#other_email_2").css({ "border": "double", "border-color": "red" });
            //                 $('<div class="other_email_2 alert alert-danger" role="alert">' + data.msg + '</div>').insertAfter("#other_email_2 label");
            //                 setTimeout(function() {
            //                     $(".other_email_2").fadeOut("slow");
            //                 }, 5000)
            //                 $(".form-edit-add :submit").attr("disabled", true);
            //             } else {
            //                 $(".form-edit-add :submit").removeAttr("disabled");
            //                 $("#other_email_2").css({ "border": "none" });
            //             }
            //         },
            //         fail: function() {
            //             alert('Ajax Request Failed');
            //         }
            //     });
            // }
    }).change();

    var x = document.getElementById("latitude");

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }

    function showPosition(position) {
        $('input[name="latitude"]').attr('value', position.coords.latitude);
        $('input[name="longitude"]').attr('value', position.coords.longitude);

        // $('input[name="latitude"]').attr("disabled", true);
        // $('input[name="longitude"]').attr("disabled", true);
    }

    function limitText(field, maxChar) {
        var ref = $(field),
            val = ref.val();
        if (val.length >= maxChar) {
            ref.val(function() {
                console.log(val.substr(0, maxChar))
                return val.substr(0, maxChar);
            });
        }
    }

});