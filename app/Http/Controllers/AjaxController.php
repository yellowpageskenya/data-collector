<?php

namespace App\Http\Controllers;

use App\Models\Ward;
use App\Models\Business;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    //
    public function getWards(Request $request){
        $district_id = $request->district_id;
        $wards = Ward::where('district_id', $district_id)->get();
        return response()->json([
            'results' => $wards,
            'status' => 'ok',
        ]);
        // dd($wards);
    }

    public function checkDuplicate(Request $request){
        $term = $request->term;

        // phone check
        $exists1 = Business::where('phone_number', '=', $term)->count();
        $exists2 = Business::where('telephone_number', '=', $term)->count();
        $exists3 = Business::where('other_phone_number_1', '=', $term)->count();
        $exists4 = Business::where('other_telephone_numbers_1', '=', $term)->count();
        $exists5 = Business::where('other_phone_number_2', '=', $term)->count();
        $exists6 = Business::where('other_telephone_numbers_2', '=', $term)->count();

        // emails check
        $exists7 = Business::where('email', '=', $term)->count();
        $exists8 = Business::where('other_email_1', '=', $term)->count();
        $exists9 = Business::where('other_email_2', '=', $term)->count();

        if ($exists1) {
            return response()->json([
                'msg' => $term." already exists in the database",
                'error' => 'true',
            ]);
        }
        if ($exists2) {
            return response()->json([
                'msg' => $term." already exists in the database",
                'error' => 'true',
            ]);
        }
        if ($exists3) {
            return response()->json([
                'msg' => $term." already exists in the database",
                'error' => 'true',
            ]);
        }
        if ($exists4) {
            return response()->json([
                'msg' => $term." already exists in the database",
                'error' => 'true',
            ]);
        }
        if ($exists5) {
            return response()->json([
                'msg' => $term." already exists in the database",
                'error' => 'true',
            ]);
        }
        if ($exists6) {
            return response()->json([
                'msg' => $term." already exists in the database",
                'error' => 'true',
            ]);
        }

        if ($exists7) {
            return response()->json([
                'msg' => $term." already exists in the database",
                'error' => 'true',
            ]);
        }
        if ($exists8) {
            return response()->json([
                'msg' => $term." already exists in the database",
                'error' => 'true',
            ]);
        }
        if ($exists9) {
            return response()->json([
                'msg' => $term." already exists in the database",
                'error' => 'true',
            ]);
        }

        return response()->json([
            'msg' => $term." does not exist in the database",
            'error' => 'false',
        ]);

        dd($exists1);
    }
}
