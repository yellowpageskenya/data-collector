<?php

namespace App\Models;

use App\Models\User;
use TCG\Voyager\Traits\Spatial;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Tu6ge\VoyagerExcel\Traits\ExcelExport;

class Business extends Model
{
    use Spatial;
    use ExcelExport;

    protected $spatial = ['location'];

    protected $guarded = [];

    public $allow_export_all = true;

    // public function __construct(array $attributes = [])
    // {
    //     parent::__construct($attributes);
    //     $this->perPage = request()->input('show') ?? 1000;
    // }

    public function district()
    {
        return $this->belongsTo('App\Models\District');
    }

    public function sector()
    {
        return $this->belongsTo('App\Models\Sector', 'category_id');
    }

    public function ward()
    {
        return $this->belongsTo('App\Models\Ward');
    }

    public function save(array $options = [])
    {
        // if (Auth::user()) {
        //     $this->created_by = Auth::user()->getKey();
        //     //you may use user's name or any other property
        //     $this->updated_by = Auth::user()->name;
        // }

        return parent::save();
    }

    public function setExportable()
    {
        //list column of table which you want to export
        //type 1 ["key1", "key2"]
        //type 2 ["key1" => "New Name", "key2" => "New Name"]
        //or with closure ["key1" => [ "name" => "New Name", "value" => function($value, $model){ return $value } ]]
        //you all free to combine three options

        return array(
            "id" => "ID",
            "updated_by" => "Updated By",
            "created_by" => array(
                "name" => "Data Collector Email",
                "value" => function ($value, $model) {
                    $user = User::where('id', (int)$value)->first();
                    $user_email = $user['email'];
                    return $user_email;
                }
            ),
            "start_time" => "Start Time",
            "date" => "Date",
            "business_name" => "Business Name",
            "type_of_business" => "Type of Business",
            "member_of_TPSF" => "Are you a Member of TPSF",
            "tpsf_clusters" => "TPSF Clusters",
            "belong_to_any_association" => "Do you belong to any established association?",
            "association_name" => "Association Name",
            "sector" => array(
                "name" => "Sector",
                "value" => function ($value, $model) {
                    return $value['name'];
                }
            ),
            "district" => array(
                "name" => "District",
                "value" => function ($value, $model) {
                    return $value['name'];
                }
            ),
            "ward" => array(
                "name" => "Ward",
                "value" => function ($value, $model) {
                    return $value['name'];
                }
            ),
            "products_services" => "Products and Services",
            "export_products_services" => "Do you Export Products / Services",
            "represented_brands" => "Represented Brands",
            "payment_methods" => "Payment Methods",
            "business_images" => array(
                "name" => "Business Images",
                "value" => function ($value, $model) {
                    $images = json_decode($value);
                    // $count = count($images);
                    return ($images);
                    // $im = "";
                    // foreach ($images as $image) {
                    //     $im .= "www.yellowpageskenya.com/" . $image . " ,";
                    // }
                    // return $im;
                }
            ),
            "hours_of_operation" => "Hours of Operation",
            "business_owner_name" => "Business Owner Name",
            "business_owner_title" => "Business Owner Title",
            "business_owner_number" => "Business Owner Mobile/Telephone",
            "business_owner_email" => "Business Owner Email",
            "phone_number" => "Mobile Number",
            "other_phone_number_1" => "Other Mobile Number",
            "other_phone_number_2" => "Other Mobile Number",
            "telephone_number" => "Telephone Number",
            "other_telephone_numbers_1" => "Other Telephone Number",
            "other_telephone_numbers_2" => "Other Telephone Number",
            "fax" => "Fax",
            "email" => "Email",
            "other_email_1" => "Other Email",
            "other_email_2" => "Other Email",
            "website" => "Website",
            "p_o_box" => "P.O. Box",
            "business_operate_known_biuilding" => "Does your Business Operate from a Known Building",
            "physical_address" => "Name of Building",
            "floor" => "Floor",
            "street_address" => "Street Name",
            "open_space_description" => "Open Space Description",
            "latitude" => "Latitude",
            "longitude" => "Longitude",
            "general_overview" => "General Overview of Industry",
            "is_a_branch" => "Is this business a branch",
            "estimated_worth_of_business" => "Estimated Worth of Business",
            "related_business_category" => "Related Business Category",
            "general_remarks" => "General Remarks",
            "end_time" => "End Time",
        );
    }

    public function getCreatedByBrowseAttribute()
    {
        $user = User::where('id', (int)$this->created_by)->first();
        return $user->name ?? 'Empty';
    }

    public function getCreatedByReadAttribute()
    {
        $user = User::where('id', (int)$this->created_by)->first();
        return $user->name ?? 'Empty';
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'created_by');
    }
}
